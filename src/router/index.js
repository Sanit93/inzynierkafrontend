import Vue from 'vue'
import Router from 'vue-router'

import Register from '@/views/auth/Register'
import Login from '@/views/auth/Login'
import Home from '@/views/Home'
import Quests from '@/views/Quests'
import Dungeons from '@/views/Dungeons'
import Arena from '@/views/Arena'

import store from '@/stores/store'

Vue.use(Router)

const ifNotAuthenticated = (to, from, next) => {
  if (!store.getters.isAuthenticated) {
    next()
    return
  }
  next('/')
}

const ifAuthenticated = (to, from, next) => {
  if (store.getters.isAuthenticated) {
    next()
    return
  }
  next('/login')
}

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      beforeEnter:ifAuthenticated
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
    },
    {
      path: '/register',
      name: 'register',
      component: Register,
    },
    {
      path: '/quests',
      name: 'quests',
      component: Quests,
    },
    {
      path: '/dungeons',
      name: 'dungeons',
      component: Dungeons,
    },
    {
      path: '/arena',
      name: 'arena',
      component: Arena,
    }
  ],
  mode: 'history'
})
