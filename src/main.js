// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from "vue";
import Vuex from "vuex";

import BootstrapVue from "bootstrap-vue";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";

import App from "./App";

import axios from "axios";

import router from "./router";
import store from "./stores/store";

axios.defaults.baseURL = "http://localhost:8888/";

Vue.use(BootstrapVue, Vuex);

axios.interceptors.response.use(undefined, function(err) {
  return new Promise(function(resolve, reject) {
    if (
      err.response.status === 403 &&
      err.config &&
      !err.config.__isRetryRequest
    ) {
      // if you ever get an unauthorized, logout the user
      store.dispatch("LOGOUT").then(success=>{
        router.push("/login")
      })
    }
    throw err;
  });
});

const token = localStorage.getItem('accessToken')
if (token) {
  axios.defaults.headers.common['Authorization'] = 'Bearer ' + token
}

Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
  el: "#app",
  router,
  store,
  components: { App },
  template: "<App/>"
});
