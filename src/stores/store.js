import Vue from "vue";
import Vuex from "vuex";

import UserModule from "./modules/user";
import UiModule from "./modules/UI";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    user: UserModule,
    ui: UiModule,
  }
});
