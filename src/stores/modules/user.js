import axios from "axios";
import jwt_decode from "jwt-decode";

export default {
  state: {
    token: localStorage.getItem("accessToken") || "",
    userId: null,
    email: null,
    character: {
      name: null,
      health: null,
      exp: null,
      level: null,
      damage: null,
      armor: null,
      strength: null,
      agility: null,
      intelligence: null,
      stamina: null,
      luck: null,
      experinceToLevel: null,
      skillPoints: null,
      gold: null,
      equipment: {
        chest: null,
        head: null,
        legs: null,
        feet: null,
        mainHand: null,
        offHand: null
      },
      items: [],
      itemsInShop: []
    }
  },
  getters: {
    isAuthenticated: state => !!state.token,
    getCharacter(state) {
      return state.character;
    },
    getItems(state) {
      return state.character.items;
    }
  },
  mutations: {
    setUser(state, payload) {
      state.userId = payload.userId;
      state.email = payload.sub;
      state.token = payload.token;
    },
    setId(state, payload) {
      state.userId = payload;
    },
    setEmail(state, payload) {
      state.email = payload;
    },
    setCharacter(state, payload) {
      state.character = payload;
    },
    logout(state) {
      (state.userId = null), (state.email = null), (state.token = null);
    }
  },
  actions: {
    LOGIN: ({ commit }, payload) => {
      var querystring = require("querystring");
      return new Promise((resolve, reject) => {
        axios
          .post(
            `auth/login`,
            querystring.stringify({
              email: payload.email,
              password: payload.password
            })
          )
          .then(({ data, status }) => {
            if (status === 200) {
              localStorage.setItem("accessToken", data);
              axios.defaults.headers.common["Authorization"] = "Bearer " + data;
              var payload = jwt_decode(data);
              payload.token = data;
              commit("setUser", payload);
              resolve(true);
            }
          })
          .catch(error => {
            reject(error);
          });
      });
    },
    REGISTER: ({ commit }, payload) => {
      return new Promise((resolve, reject) => {
        axios
          .post("/auth/register", payload)
          .then(({ data, status }) => {
            if (status == 200) {
              commit("setCharacter", data.character);
              resolve(true);
            }
          })
          .catch(error => {
            reject(error);
          });
      });
    },
    LOGOUT: ({ commit }) => {
      return new Promise(resolve => {
        localStorage.removeItem("accessToken");
        commit("logout");
        resolve;
      });
    },
    FETCH_CHARACTER: ({ commit }) => {
      return new Promise((resolve, reject) => {
        axios
          .get("/characters")
          .then(({ data, status }) => {
            if (status == 200) {
              commit("setCharacter", data);
              resolve(true);
            }
          })
          .catch(error => {
            reject(error);
          });
      });
    },
    GOTO_QUEST: ({}, payload) => {
      return new Promise((resolve, reject) => {
        axios
          .get("/characters/quest/" + payload)
          .then(({ data, status }) => {
            if (status === 200) {
              resolve(data);
            }
          })
          .catch(error => {
            reject(error);
          });
      });
    },
    GOTO_ARENA: () => {
      return new Promise((resolve, reject) => {
        axios
          .get("/characters/arena")
          .then(({ data, status }) => {
            if (status === 200) {
              resolve(data);
            }
          })
          .catch(error => {
            console.log(error.response);
            reject(error);
          });
      });
    },
    SELL_ITEM: ({ commit }, payload) => {
      return new Promise((resolve, reject) => {
        axios
          .get("/characters/sell/" + payload)
          .then(({ data, status }) => {
            if (status === 200) {
              commit("setCharacter", data);
              resolve(data);
            }
          })
          .catch(error => {
            console.log(error.response);
            reject(error);
          });
      });
    },
    EQUIP_ITEM: ({ commit }, payload) => {
      return new Promise((resolve, reject) => {
        axios
          .get("/characters/equip/" + payload)
          .then(({ data, status }) => {
            if (status === 200) {
              commit("setCharacter", data);
              resolve(data);
            }
          })
          .catch(error => {
            console.log(error.response);
            reject(error);
          });
      });
    },
    INCREASE_STAT: ({ commit }, payload) => {
      return new Promise((resolve, reject) => {
        axios
          .get("/characters/increase/" + payload)
          .then(({ data, status }) => {
            if (status === 200) {
              commit("setCharacter", data);
              resolve(data);
            }
          })
          .catch(error => {
            console.log(error.response);
            reject(error);
          });
      });
    }
  }
};
