export default {
  state: {
    itemComponentId: 0,
  },
  getters: {
    itemComponentId: function(state) {
        return state.itemComponentId;
    }
  },
  mutations: {
    increment: function(state) {
        state.itemComponentId++;
    }
  },
  actions: {   
  }
};
